
# Первая загрузка

npm install

npm install -g gulp
(gulpfile.js > RMB > Show gulp task)

npm install -g bower

bower install breakpoint-sass
(bower install breakpoint-sass --save-dev)

#Пдагины vendor

slick - ver 1.5.9 (http://kenwheeler.github.io/slick/)
modernizr - 2.5.3 (http://modernizr.com/)







#Установка ядра Gulp

*При первой установке следует установить gulp глобально для удобства работы*

`sudo npm install -g gulp`

*После необходимо выполнить сравнение версий для избежания конфликтов*

`gulp -v`

*Если версии совпадают - все ок, если нет - необходимо ручками заинсталлить верную глобальную или локальную версию*

*Пример - нужно установить версию 3.8.10*

`npm install --save-dev gulp@3.8.10`

*или*

`npm install -g gulp@3.8.10`

либо заапдейтить версии другими способами

#Установка плагинов:

*Все зависимости прописаны в package.json-файле*

`npm install`

#Установка сервера
`sudo npm install http-server -g`

#Запуск сервера
`http-server`
или
`npm start`

Сайт будет доступен по адресу http://localhost:8080/

#Дополнительно:

*Запуск дефолтного таска*
`gulp`

*Запуск релизного таска*
`gulp prod`

____


#backgroungs
http://subtlepatterns.com/
http://www.transparenttextures.com/

#gradients
http://uigradients.com/