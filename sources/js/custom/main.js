var app;
(function(){
	app = {
		module: {

			checkForm: function(form){
				var $el = $(form),
					$sbmt = $el.find('[type=submit]'),
					wrong = false,
					mes = '';

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form-field'),
						val = $.trim($t.val()),
						rexp = '';
					$wrap.removeClass('error');

					if ( $t.attr('type') == 'checkbox' && !$t.is(':checked') ){
						val = false;
					}

					switch (type) {
						case 'number':
							rexp = /^\d+$/i;
							break;
						case 'letter':
							rexp = /\w+$/i;
							break;
						case 'email':
							rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
							break;
						default:
							rexp = /.+$/igm;
					}

					if ( !rexp.test(val) || val == '0'){
						wrong = true;
						$wrap.addClass('error');
					} else {
						$wrap.addClass('success');
					}

				});


				return !!wrong;

			},

			counter: {
				check: function(){
					$('[data-count]').each(function(){
						var $t = $(this), $w = $(window), t = $t.offset().top, st = $w.height() - ($t.height()*2);

						if ( t <= st ) $t.counter().removeAttr('data-count');

					});
				}
			},

			tooltip: {
				init: function(){
					var $tooltip = $('.tooltip');

					if ( !$tooltip.size() ){
						$tooltip = $('<div/>', {'class': 'tooltip'}).appendTo('body');
					}

					$('body').on('mouseover', '[data-tooltip]', function(){
						var $t = $(this);
						var text = $t.attr('data-tooltip');
						$tooltip.html('<span class="tooltip-inner">'+text+'</span>').stop(true, true).delay(1000).addClass('show');

						var top = $t.offset().top + $t.outerHeight();
						var left = $t.offset().left + ($t.outerWidth()/2);
						$tooltip.css({top: top, left: left});

					}).on('mouseleave', '[data-tooltip]', function(){
						$tooltip.removeClass('show');
					});


				}
			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup-inner">'
						+				'<div class="popup-layout">'
						+					'<div class="popup-close"></div>'
						+					'<div class="popup-content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup-overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup-content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes){
					var html = '<div class="popup-text">' + mes + '</div>'
						+	'<div class="popup-link"><span class="js-popup-close btn btn_green">Ок</span></div>';
					self.popup.open('.popup_info', html);
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				}
			},

			bottomStick: {
				event: function(){
					$('.js-bottom-stick').each(function(){
						var t = $(this).offset().top;
						var h = $('.wrapper').height();
						$(this).height(h - t);
					});
				},
				init: function(){
					self.bottomStick.event();
					$(window).resize(self.bottomStick.event);
				}
			},

			scrollBar: {

				init: function(){
					$('.js-scroll-wrap').not('.js-scroll-inited').mCustomScrollbar({
						callbacks:{
							onCreate: function(){
								$(this).addClass('js-scroll-inited');
							},
							whileScrolling:function(){
								self.counter.check();

								if ( typeof contentScrollEvent === 'function') contentScrollEvent(this.mcs);
							}
						}
					});
				}
			},

			slider: {

				init: function(){

					$('.js-page-slider').each(function(){

						var $slider = $(this);
						var $prev = $slider.closest('.js-slider-wrap').find('.control-link_arrow-left');
						var $next = $slider.closest('.js-slider-wrap').find('.control-link_arrow-right');

						$slider.slick({
							slidesToShow: 4,
							slidesToScroll: 1,
							prevArrow: $prev,
							nextArrow: $next,
							responsive: [
								{
									breakpoint: 1700,
									settings: {
										slidesToShow: 3
									}
								}
							]
						});
					});
				}
			},

			gallery: {

				init: function(initialSlide){

					var is = initialSlide || 0;

					var $picture = $('.js-gallery-picture');
					var $preview = $('.js-gallery-preview');

					var $prev = $preview.closest('.gallery').find('.gallery-preview__arrow_prev');
					var $next = $preview.closest('.gallery').find('.gallery-preview__arrow_next');


					$picture.slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						fade: true,
						asNavFor: $preview,
						initialSlide: is
					});

					$preview.slick({
						slidesToShow: 6,
						slidesToScroll: 1,
						asNavFor: $picture,
						//centerMode: true,
						focusOnSelect: true,
						prevArrow: $prev,
						nextArrow: $next,
						initialSlide: is
					});

				}
			},

			mobileRatio: {

				resize: function(el){
					var $w = $(window);
					var $d = $(document);

					if ( Modernizr.touch ){
						$(el).css('min-height', ($w.height() / $w.width()) * $d.width());
					}

				},

				init: function(el){
					self.mobileRatio.resize(el);
					$(window).resize(function(){
						self.mobileRatio.resize(el);
					});
				}
			},

			welcomeSlider:{
				init: function(){
					var $slide = $('.welcome-slider__item');
					if ( $slide.size() ){
						var num = $slide.length;

						setInterval(function(){
							var $cur = $slide.filter('.active');
							var n = $cur.index();

							$cur.removeClass('active');
							$slide.eq((n + 1) % num).addClass('active');

						}, 8000);
					}
				}
			},

			filter: {
				test_disabled: function (element) {
					return $(element).parent().hasClass ('disabled') || $(element).parent ().parent ().hasClass ('disabled');
				},
				find_conjunction: function (arr1, arr2) {
					var i;
					if (arr1.length && arr2.length) {
						for (i=0; i<arr1.length; i++)
							if (arr2.indexOf(arr1 [i]) != -1)
								return true;
					}
					else
						return true;
					return false;
				},
				fconjunction: function (arr1, arr2) {
					var i;
					for (i=0; i<arr1.length; i++)
						if (arr2.indexOf(arr1 [i]) == -1)
							arr2.push (arr1 [i]);
					return arr2;
				},
				fconjunction_new: function (arr1, arr2) {
					var i;
					var result = [];
					for (i=0; i<arr1.length; i++)
						if (arr2.indexOf(arr1 [i]) != -1)
							result.push (arr1 [i]);
					return result;
				},
				get_all_by_type: function (type) {
					var elements = $('[data-type='+type+']'), element, i, result = [];
					for (i=0; i<$(elements).length; i++) {
						element = $(elements) [i];
						result.push ($(element).attr ('name'));
					}
					return result;
				},
				get_all_by_type_elements: function (type) {
					var elements = $('[data-element='+type+']'), i, result = [];
					for (i=0; i<$(elements).length; i++) {
						result.push ($(elements) [i]);
					}
					return result;
				},
				match_filters: function (elements, filters_result, type) {
					var el, i, k, val, parent;
					for (k in elements) {
						if (k != type) { // если фильтр неотмечен, видоизменяем
							for (i=0; i<elements [k].length; i++) {
								el = elements [k] [i];
								val = $(el).attr ('name');
								if (filters_result [k].indexOf (val) == -1) {
									if (! $(el).hasClass ('disabled'))
										$(el).addClass ('disabled');
								}
								else
									$(el).removeClass ('disabled');
							}
						}
						/*else {
						 // если фильтр отмечен - включаем все пункты
						 for (i=0; i<elements [k].length; i++) {
						 el = elements [k] [i];
						 $(el).removeClass ('disabled');
						 }
						 }*/
					}
				},
				go: function (clicked_element) {
					var elements = {'clinic': [], 'town': [], 'prices': [], 'response_time': []};
					var filters = {'clinic': [], 'town': [], 'prices': [], 'response_time': []};
					var filters_all = {'clinic': [], 'town': [], 'prices': [], 'response_time': []};
					var filters_result = {'clinic': [], 'town': [], 'prices': [], 'response_time': []};
					var i, k, el, filter_data;
					var exists_items = [];
					for (k in filters) {
						switch (k) {
							case 'clinic':
								for( i=0; i < $('.input_select').length; i++ ) {
									el = $('.input_select') [i];
									if ($(el).is (":checked"))
										filters [k].push ($(el).attr ('name'));
								}
								break;
							case 'town': case 'prices': case 'response_time':
							for( i=0; i < $('[name='+k+']').length; i++ ) {
								el = $('[name='+k+']') [i];
								if ($(el).val () != 0)
									filters [k].push ($(el).val ());
							}
							break;
						}
						elements [k] = this.get_all_by_type_elements (k);
						filters_all [k] = this.get_all_by_type (k);
					}
					for ( i=0; i < $('.object-item').length; i++ ) {
						el = $('.object-item') [i];
						filter_data = $(el).data ('filter').split (",");
						$(el).addClass ('hidden');
						if (this.find_conjunction(filters ['clinic'], filter_data) &&
							this.find_conjunction(filters ['town'], filter_data) &&
							this.find_conjunction(filters ['prices'], filter_data) &&
							this.find_conjunction(filters ['response_time'], filter_data)) {
							$(el).removeClass ('hidden');
							exists_items = this.fconjunction (exists_items, filter_data);
						}
					}
					for ( k in filters ) {
						filters_result [k] = this.fconjunction_new (exists_items, filters_all [k]);
						console.log (filters_result [k]);
					}

					if (typeof $(clicked_element).data('type') != 'undefined') {
						// фильтрация
						this.match_filters (elements, filters_result, $(clicked_element).data('type'));
					}
					if (typeof $(clicked_element).data('clear') != 'undefined') {
						// сброс
						this.match_filters (elements, filters_result, $(clicked_element).data('clear'));
					}
				}
			},

			loadContent: function(url){
				var $slide = $('.content-aside_slide'),
					$inner = $slide.find('.content-wrap').find('.inner');

				$slide.addClass('content-aside_show');
				$inner.removeClass('show').html('');

				$.ajax({
					url: url,
					cache: false
				}).done(function(data){
					$inner.addClass('show').html(data);
					$inner.find('[data-required=phone]').mask('+7 (999) 999-99-99');
				}).fail(function(err){
					$inner.addClass('show').html('Ошибка - ' + err.statusText);
				});
			}

		},
		init: function() {

			//self.viewPort.init();
			//self.preLoad.init();
			self.mobileRatio.init('.wrapper');
			self.tooltip.init();
			self.scrollBar.init();
			self.slider.init();
			self.welcomeSlider.init();
			self.bottomStick.init();


			$('.js-form-phone').mask('+7 (999) 999-99-99');
			$('[data-required=phone]').mask('+7 (999) 999-99-99');

			$('body').on('click', '.content-close, .js-content-close', function(e){
				e.preventDefault();
				$(this).closest('.content-aside_show').removeClass('content-aside_show').find('.content-wrap').find('.inner').removeClass('show');
			}).on('click', '.js-load-content', function(e){
				e.preventDefault();
				var url = $(this).attr('href') || $(this).data('href');
				self.loadContent(url);
			}).on('click', '.aside-menubtn', function(e){
				e.preventDefault();
				var $t = $(this);
				if ( !$t.hasClass('loading') ){
					$t.addClass('loading')
					.toggleClass('active')
					.next('.aside-nav').fadeToggle(300, function(){
						$t.removeClass('loading');
					});
				}
			}).on('click', '.aside-lang', function(e){
				e.preventDefault();
				var $t = $(this);
				if ( !$t.hasClass('loading') ){
					$t.addClass('loading')
					.toggleClass('active')
					.next('.aside-nav').fadeToggle(300, function(){
						$t.removeClass('loading');
					});
				}
			}).on('click', '.filter-input', function(e){
				//e.preventDefault();
				var $t = $(this);
				var $item = $t.closest('.filter-item');
				var $drop = $item.find('.filter-dropdown');
				var h = $(window).height();
				$item.siblings().removeClass('active').find('.filter-dropdown').fadeOut(300);

				if ( !$t.hasClass('loading') ){


					$t.addClass('loading');
					$item.toggleClass('active');
					$drop.fadeToggle(300, function(){
						$t.removeClass('loading');
					});

					//console.info( h - $drop.offset().top );
					if ( h - $t.offset().top < 240 ){
						$drop.addClass('up');
					} else {
						$drop.removeClass('up');
					}
				}

			}).on('mousedown', function(e){
				if ( $('.filter-dropdown:visible').size() ){
					if ( $(e.target).closest('.filter-item').length ) return false;
					$('.filter-item').removeClass('active').find('.filter-dropdown').fadeOut(300);
				}
			}).on('mouseover', '.minig-hendler__item', function(e){
				var $t = $(this);
				var $w = $t.closest('.minig');
				var ind = $t.index();

				$t.addClass('active').siblings('.minig-hendler__item').removeClass('active');
				$w.addClass('hover').find('.minig-gallery__item').eq(ind).addClass('active').siblings('.minig-gallery__item').removeClass('active');

			}).on('mouseleave', '.minig', function(e){
				var $t = $(this);
				$t.removeClass('hover').find('.minig-gallery__item').removeClass('active');
				$t.find('.minig-hendler__item').removeClass('active');
			})

/**
 * filter
 */

			.on('change', '.filter-dropdown__checkbox input', function(e){
				if (self.filter.test_disabled (this)) { $(this).prop ('checked', false); return; }
				var $t = $(this),
					$group = $t.closest('.filter-item'),
					$res = $group.find('.js-result'),
					checked = $group.find('input:checked').length;

				if ( checked ){
					$group.addClass('filter-item_filled');
					$res.text(checked);
				} else {
					$group.removeClass('filter-item_filled');
					$res.text('');
				}
				self.filter.go (this);
			}).on('click', '.filter-dropdown__link', function(e){
				if (self.filter.test_disabled (this)) return;
				var $t = $(this),
					$group = $t.closest('.filter-item'),
					$res = $group.find('.js-result'),
					$input = $group.find('.input_choose'),
					val = $t.data('val'),
					desc = $t.data('desc');

				$group.addClass('filter-item_filled');
				$input.val(val);
				$res.text(desc);
				self.filter.go (this);
			}).on('click', '.filter-clear', function(e){
				var $t = $(this),
					$group = $t.closest('.filter-item'),
					$res = $group.find('.js-result'),
					$input = $group.find('.input_choose'),
					val = $t.data('val'),
					desc = $t.data('desc');

				$group.removeClass('filter-item_filled');
				$input.val(0);
				$res.text('');
				$group.find('input:checked').prop('checked', false);
				self.filter.go (this);
			}).on('click', '.js-tab-link', function(e){
				var $t = $(this),
					$wrap = $t.closest('.js-tab-wrap'),
					$tab = $wrap.find('.js-tab-item'),
					$link = $wrap.find('.js-tab-link'),
					ind = $t.index();

				if ( !$t.hasClass('active') ){
					$t.addClass('active').siblings().removeClass('active');
					$tab.eq(ind).addClass('active').siblings().removeClass('active');
					$link.eq(ind).addClass('active').siblings().removeClass('active');
				}


			}).on('click', '.js-call-toggle', function(e){
				var $t = $(this);
				$t.closest('.inner__feedback')
					.find('.form_feedback')
					.add('.inner__feedback-contact')
					.toggleClass('hidden')

			})


			.on('mouseover', '[data-hover-link]', function(e){
				if (self.filter.test_disabled (this)) return;
				var $t = $(this),
					data = $t.data('hover-link');
				if ( $(data).prop('tagName') == 'path' ){
					$(data).attr('class','hover');
				} else {
					$(data).addClass('hover');
				}
			}).on('mouseleave', '[data-hover-link]', function(e){
				if (self.filter.test_disabled (this)) return;
				var $t = $(this),
					data = $t.data('hover-link');
				if ( $(data).prop('tagName') == 'path' ){
					$(data).attr('class',' ');
				} else {
					$(data).removeClass('hover');
				}
			})

			.on('keyup', 'input', function(e){
				if ( $(this).closest('.error').size() ) $(this).closest('.error').removeClass('error');
			}).on('submit', 'form', function(e){
				e.preventDefault();
				var $form = $(this);
				if ( self.checkForm($form) ){
					$form.addClass('form_error');
					return false;
				} else {
					$form.removeClass('form_error');

					$.ajax({
						url: $form.attr('action'),
						method: $form.attr('method'),
						data: $form.serialize()
					}).success(function (data) {
						if (data.status == 'access') $form.addClass('form_sent').find('.form-message').text(data.message);
					}).complete(function (xhr, textStatus) {
						//console.info(xhr);
					});

					//$form.submit();
				}
			})

			;


		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();



$(function(){
	$.fn.counter = function(){
		var $t = $(this), val = +$t.text(), timer, range = $t.data('count'), time = 1500 ;

		if (range == undefined|| val == undefined) {
			console.warn('Аларм!!!');
			return this;
		}

		timer = setInterval(function () {
			$t.text(val); val++;
			if ( val > range ){
				clearTimeout(timer);
			}
		}, (time/range));

		return this;

	};
});