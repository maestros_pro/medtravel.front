<?php
header('Access-Control-Allow-Origin: *');
sleep(1);

$data = $_POST;


$city001 = '[
	{
		"name":"«Клуб Велход»",
		"site":"www.velhod.ru",
		"place":"59.889611, 30.289673",
		"info":"<p>Москва, ул. Смольная, 63Б, 1-й этаж здания «Водный мир», павильон М2</p><p>Тел. 8 (495) 22-33-112 (доб. 2)</p><p>velhod@mail.ru</p>"
	},
	{
		"name":"Компания «Прокатись.ru»",
		"site":"www.prokatis.ru",
		"place":"59.889611, 30.289673",
		"info":"<p>Москва, МКАД 14 км, Внешняя сторона. Московская область, г. Котельники, Коммерческий проезд, строение 6</p><p>Тел. 8 (495) 215-08-31</p>"
	},
	{
		"name":"Магазин «13 Калибр»",
		"site":"info@13k.ru",
		"place":"59.889611, 30.289673",
		"info":"<p>13 км от МКАД по Ленинградскому шоссе, Солнечногорский р-н, д. Черная Грязь, д. 3</p><p>Тел. 8 (495) 776-34-76</p><p>info@13k.ru</p>"
	},
	{
		"name":"Магазин «АКВА спорт»",
		"site":"www.acvasport.ru",
		"place":"59.889611, 30.289673",
		"info":"<p>Балашиха, ул. Транспортная, вл 3а, стр 1, 500 метров от МКАД</p><p>Тел. 8 (495)973-00-88</p>"
	}
]';

$items1 = '[
    {
        "id":"00001",
        "icon":"img/video-icon-dog.svg",
        "video":"9K407EhHBiU",
        "videoPreview":"https://i.ytimg.com/vi/LOQdoTvKHwg/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=2EaHPdJcPWJGe_NHceIaa3kmMK4",
        "preTitle": "собаки",
        "title":"Самый игривый разбойник",
        "rating":"128",
        "date":"12 января 2016",
        "likes":"100",
        "isFavorite":"false",
        "tags":[
            {
                "name":"собаки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание собак",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo": {
            "id":"null",
            "text":"null"
        },
        "nextVideo": {
            "id": "00002",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00002",
        "icon":"img/video-icon-dog.svg",
        "video":"zeilLQXOhfM",
        "videoPreview":"https://i.ytimg.com/vi/x090xTMqz0s/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=0v799PFpwXmHBhI9CwhUVL3hPmo",
        "preTitle": "собаки",
        "title":"пушистый клубок",
        "rating":"113",
        "date":"13 января 2016",
        "likes":"120",
        "isFavorite":"false",
        "tags":[
            {
                "name":"собаки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание собак",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00001",
            "text": "Предыдущее видео"
        },
        "nextVideo": {
            "id": "00003",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00003",
        "icon":"img/video-icon-dog.svg",
        "video":"U-P-iGsj4Zg",
        "videoPreview":"https://i.ytimg.com/vi/vM4VwFJnB4U/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=VlSW-YNgOvBcCcuLg3YuPIU0pTs",
        "preTitle": "собаки",
        "title":"Эгейский рыжий член семьи",
        "rating":"106",
        "date":"14 января 2016",
        "likes":"180",
        "isFavorite":"true",
        "tags":[
            {
                "name":"собаки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание собак",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00002",
            "text": "Предыдущее видео"
        },
        "nextVideo": {
            "id": "00004",
            "text": "Следующее видео"
        }
    }
]';

$items2 = '[
    {
        "id":"00008",
        "icon":"img/video-icon-cat.svg",
        "video":"XnjR4Aw3bi0",
        "videoPreview":"https://i.ytimg.com/vi/7ztqazxvxmc/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=g8RfNkNZyiWQe-sSfaUqVSJHYww",
        "preTitle": "кошки",
        "title":"Самый игривый разбойник",
        "rating":"128",
        "date":"19 января 2016",
        "likes":"500",
        "isFavorite":"true",
        "tags":[
            {
                "name":"кошки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание кошек",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id":"null",
            "text":"null"
        },
        "nextVideo": {
            "id": "00009",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00009",
        "icon":"img/video-icon-cat.svg",
        "video":"pPmgrQoNwvU",
        "videoPreview":"https://i.ytimg.com/vi/rScIHgP8ywg/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=PTsZeIdjLlNb7oGlBwuaHFqqkR0",
        "preTitle": "кошки",
        "title":"пушистый клубок",
        "rating":"113",
        "date":"20 января 2016",
        "likes":"150",
        "isFavorite":"false",
        "tags":[
            {
                "name":"кошки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание кошек",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00008",
            "text": "Предыдущее видео"
        },
        "nextVideo": {
            "id": "00010",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00010",
        "icon":"img/video-icon-cat.svg",
        "video":"4H8pYq_rZCM",
        "videoPreview":"https://i.ytimg.com/vi/kgaIbDvP_KI/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=VDu_6mT8HXfGDBlkj5likSe60oI",
        "preTitle": "кошки",
        "title":"Эгейский рыжий член семьи",
        "rating":"106",
        "date":"21 января 2016",
        "likes":"180",
        "isFavorite":"false",
        "tags":[
            {
                "name":"кошки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание кошек",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00009",
            "text": "Предыдущее видео"
        },
        "nextVideo": {
            "id": "00011",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00011",
        "icon":"img/video-icon-cat.svg",
        "video":"XnjR4Aw3bi0",
        "videoPreview":"https://i.ytimg.com/vi/7ztqazxvxmc/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=g8RfNkNZyiWQe-sSfaUqVSJHYww",
        "preTitle": "кошки",
        "title":"Багира из диких джнунглей",
        "rating":"103",
        "date":"22 января 2016",
        "likes":"80",
        "isFavorite":"true",
        "tags":[
            {
                "name":"кошки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание кошек",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00010",
            "text": "Предыдущее видео"
        },
        "nextVideo": {
            "id": "00012",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00012",
        "icon":"img/video-icon-cat.svg",
        "video":"pPmgrQoNwvU",
        "videoPreview":"https://i.ytimg.com/vi/rScIHgP8ywg/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=PTsZeIdjLlNb7oGlBwuaHFqqkR0",
        "preTitle": "кошки",
        "title":"Хвостатый ленивец",
        "rating":"103",
        "date":"23 января 2016",
        "likes":"140",
        "isFavorite":"false",
        "tags":[
            {
                "name":"кошки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание кошек",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00011",
            "text": "Предыдущее видео"
        },
        "nextVideo": {
            "id": "00013",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00013",
        "icon":"img/video-icon-cat.svg",
        "video":"4H8pYq_rZCM",
        "videoPreview":"https://i.ytimg.com/vi/kgaIbDvP_KI/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=VDu_6mT8HXfGDBlkj5likSe60oI",
        "preTitle": "кошки",
        "title":"пушистый клубок",
        "rating":"101",
        "date":"24 января 2016",
        "likes":"100",
        "isFavorite":"false",
        "tags":[
            {
                "name":"кошки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание кошек",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00012",
            "text": "Предыдущее видео"
        },
        "nextVideo": {
            "id": "00014",
            "text": "Следующее видео"
        }
    },
    {
        "id":"00014",
        "icon":"img/video-icon-cat.svg",
        "video":"XnjR4Aw3bi0",
        "videoPreview":"https://i.ytimg.com/vi/7ztqazxvxmc/hqdefault.jpg?custom=true&w=196&h=110&stc=true&jpg444=true&jpgq=90&sp=68&sigh=g8RfNkNZyiWQe-sSfaUqVSJHYww",
        "preTitle": "кошки",
        "title":"Усы, лапы и длинный хвост",
        "rating":"100",
        "date":"25 января 2016",
        "likes":"100",
        "isFavorite":"true",
        "tags":[
            {
                "name":"кошки",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            },
            {
                "name":"воспитание кошек",
                "url":"https://hillsclub.ru/cat/promo/petstop/"
            }
        ],
        "prevVideo":{
            "id": "00013",
            "text": "Предыдущее видео"
        },
        "nextVideo":{
            "id":"null",
            "text":"null"
        }
    }
]';

$json1 = '{
    "status": "success",
    "register": true,
    "data": {
         "items": '.$items1.',
         "userlogged":true,
         "isLast":true,
         "preloader":"/img/loader_blue.svg",
         "subTitle": "Для собак"
     }
 }';

$json2 = '{
    "status": "success",
    "register": true,
    "data": {
         "items": '.$items2.',
         "userlogged":true,
         "isLast":true,
         "preloader":"/img/loader_red.svg",
         "subTitle": "Для кошек"
     }
 }';


if ($data['cat']) {
	$json = $json2;
} else {
	$json = $json1;
}

echo $json;
?>