// Nodeapp gulpfile
var publicDir = './public';
var sourcesDir = './sources';

// System
var path = require('path');
var fs = require('fs');

// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var optipng = require('imagemin-optipng');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var merge = require('merge-stream');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
//var order = require('gulp-order');
var newer = require('gulp-newer');
var prettify = require('gulp-prettify');
var jade = require('gulp-jade');
var bower = require('gulp-bower');
var notify = require('gulp-notify');

var sassConfig = {
	sassPath: './sources/css',
	bowerDir: './bower_components'
};


// Error Handler (continue on error)
function handleError(err) {

	console.log(err.toString());
	this.emit('end');

	return false;
}


// JS -> Lint
gulp.task('lint', function () {
	return gulp.src(['sources/js/**/*.js', '!sources/js/vendor/**/*.js', '!sources/js/require.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

// FONTS -> FONTS
gulp.task('fonts', function () {
	return gulp.src(['sources/fonts/**/*'])
		.pipe(newer(publicDir + '/fonts'))
		.pipe(gulp.dest(publicDir + '/fonts'));
});

// data -> data
gulp.task('data', function () {
	return gulp.src(['sources/data/*'])
		.pipe(newer(publicDir + '/data'))
		.pipe(gulp.dest(publicDir + '/data'));
});


// HTML -> HTML
gulp.task('html', function () {
	return gulp.src('sources/*.html')
		.pipe(prettify({indent_char: '	', indent_size: 1}))
		.pipe(gulp.dest(publicDir + '/'));
});


// Jade -> HTML
gulp.task('jade', function () {
	return gulp.src('sources/*.jade')
		.pipe(jade({
			pretty: true
		})).on('error', handleError)
		.pipe(prettify({indent_char: '	', indent_size: 1}))
		.pipe(gulp.dest(publicDir + '/'));
});

// JadeContent -> HTML
gulp.task('jadeContent', function () {
	return gulp.src('sources/content/*.jade')
		.pipe(jade({
			pretty: true
		})).on('error', handleError)
		.pipe(prettify({indent_char: '	', indent_size: 1}))
		.pipe(gulp.dest(publicDir + '/content/'));
});

// SASS -> CSS
gulp.task('sass', function () {
	return gulp.src(sassConfig.sassPath + '/style.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compact'
		}).on("error", notify.onError(function (error) {
			return "Error: " + error.message;
		})))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(publicDir + '/css'));
});

// SASS -> CSSmin
gulp.task('sass-min', function () {
	return gulp.src(sassConfig.sassPath + '/style.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed'
		}).on("error", notify.onError(function (error) {
			return "Error: " + error.message;
		})))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(publicDir + '/css'));
});

// IMAGES -> IMAGES.MIN
gulp.task('images-min', function () {
	return gulp.src(['sources/img/**/*'])
		.pipe(newer(publicDir + '/img'))
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [optipng()]
		})).on('error', handleError)
		.pipe(gulp.dest(publicDir + '/img'));
});

// IMAGES -> IMAGES
gulp.task('images', function () {
	return gulp.src(['sources/img/**/*'])
		.pipe(newer(publicDir + '/img'))
		.pipe(gulp.dest(publicDir + '/img'));
});

// JS -> JS
gulp.task('scripts', function() {

	var vendor = gulp.src('sources/js/vendor/*.js')
		.pipe(concat('vendor.js'))
		.on('error', handleError)
		.pipe(gulp.dest(publicDir + '/js'));

	var custom = gulp.src('sources/js/custom/*.js')
		.pipe(concat('custom.js'))
		.on('error', handleError)
		.pipe(gulp.dest(publicDir + '/js'));

	var common = gulp.src(['sources/js/**/*.js', '!sources/js/vendor/**/*.js', '!sources/js/custom/**/*.js'])
		.pipe(newer(publicDir + '/js'))
		.pipe(gulp.dest(publicDir + '/js'));

	return merge(vendor, custom, common);

});

// JS -> JS.min
gulp.task('scripts-min', function() {

	var vendor = gulp.src('sources/js/vendor/*.js')
		.pipe(concat('vendor.js'))
		.pipe(uglify())
		.on('error', handleError)
		.pipe(gulp.dest(publicDir + '/js'));

	var custom = gulp.src('sources/js/custom/*.js')
		.pipe(concat('custom.js'))
		.pipe(uglify())
		.on('error', handleError)
		.pipe(gulp.dest(publicDir + '/js'));

	var common = gulp.src(['sources/js/**/*.js', '!sources/js/vendor/**/*.js', '!sources/js/custom/**/*.js'])
		.pipe(newer(publicDir + '/js'))
		.pipe(gulp.dest(publicDir + '/js'));

	return merge(vendor, custom, common);

});

gulp.task('createInnerDirs', function(){

	var jsDir = ['custom', 'vendor'];

	for (var index in jsDir){
		if (jsDir.hasOwnProperty(index)){
			var result;

			fs.exists(sourcesDir + '/js/' + jsDir[index], function(exists){
				result = exists
			});

			if (!result){
				fs.mkdir(sourcesDir + '/js/' + jsDir[index], function(err){
					if (err !== null){
						console.log('dir is already exists');
					}
				});
			}
		}
	}

	return false;
});


// Watch Files For Changes
gulp.task('watch', function () {
	gulp.watch(['sources/js/**/*.js', '!sources/js/vendor/**/*.js', '!sources/js/require.js'], ['lint']);
	gulp.watch(['sources/js/*.js', 'sources/js/**/*.js'], ['scripts']);
	gulp.watch('sources/css/**/*.scss', ['sass']);
	gulp.watch('sources/fonts/*', ['fonts']);
	gulp.watch('sources/data/*', ['data']);
	gulp.watch('sources/img/**/*', ['images']);
	gulp.watch(['sources/*.jade', 'sources/layout/*.jade', 'sources/layout/**/*.jade'], ['jade']);
	gulp.watch(['sources/content/*.jade'], ['jadeContent']);
	gulp.watch('sources/*.html', ['html']);
});

// Default Task
gulp.task('default', ['dev']);
gulp.task('dev', ['lint', 'sass', 'scripts', 'createInnerDirs', 'images', 'fonts', 'data', 'jade', 'jadeContent', 'watch']);
gulp.task('prod', ['lint', 'sass-min', 'scripts-min', 'createInnerDirs', 'images-min', 'fonts', 'data', 'jade', 'jadeContent']);